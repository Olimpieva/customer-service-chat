import React from 'react';
import { useSelector } from 'react-redux';
import { Navigate, Route, Routes } from 'react-router-dom';

import { currentUserSelector } from '../../redux/selectors';
import Login from '../Login/Login';

import './App.css';
import '../../fonts/fontawesome';

function App() {

  const user = useSelector(currentUserSelector);

  return (
    <div className="app">
      <Routes>
        <Route path="/" element={user.data ? <div>Main Page</div> : <Navigate replace to="/signin" />} /> {/* just to show a successful login */}
        <Route path="/signin" element={user.data ? <Navigate replace to="/" /> : <Login />} />
        <Route path="*" element={<div>Page Not Found</div>} />
      </Routes>
    </div>
  );
};

export default App;
