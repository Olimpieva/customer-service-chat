import React from "react";

import './FormError.css'

function FormError(props) {
    const { name, message } = props;

    return (
        <span
            className={`form-error ${message && 'form-error_active'}`}
            id={`${name}-error`}
        >
            {message}
        </span>
    )

}

export default FormError;