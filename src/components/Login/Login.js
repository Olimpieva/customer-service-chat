import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useFormik } from 'formik';
import { getFirestore } from "firebase/firestore";

import { firebaseApp } from '../../index';
import { login } from '../../redux/actions';
import { currentUserSelector } from '../../redux/selectors';
import { loginValidationSchema } from '../../utils/validation';
import InputField from '../InputField/InputField';
import FormError from '../FormError/FormError';
import Header from '../Header/Header';

import './Login.css';

function Login() {

    const dispatch = useDispatch();
    const { error } = useSelector(currentUserSelector)

    const loginFormInitialValues = { email: '', password: '' };

    const loginFormSubmit = async ({ email, password }) => {
        await dispatch(login({ email, password }));
    };

    const { values: { email, password }, handleChange, handleSubmit, handleBlur: handleFocus, touched, errors, setErrors, isValid, isSubmitting } = useFormik({
        initialValues: loginFormInitialValues,
        validationSchema: loginValidationSchema,
        onSubmit: values => loginFormSubmit(values),
    });

    useEffect(() => {
        getFirestore(firebaseApp);
    }, []);

    useEffect(() => {

        if (error) {
            setErrors({ login: error });
        }

    }, [error, setErrors]);

    return (
        <div className='login-page'>
            <Header />
            <main className='login'>
                <h1 className="login__title">Авторизация</h1>

                <form className='login__form' data-testid='login__form' onSubmit={handleSubmit}>
                    <InputField
                        type="email"
                        formName="login"
                        name="email"
                        title="Email"
                        value={email}
                        onChange={handleChange}
                        onFocus={handleFocus}
                        error={touched.email ? errors.email : ''}
                    />
                    <InputField
                        type="password"
                        formName="login"
                        name="password"
                        title="Пароль"
                        value={password}
                        onChange={handleChange}
                        onFocus={handleFocus}
                        error={touched.password ? errors.password : ''}
                    />
                    <button className='login__button_submit' type='submit' disabled={!isValid || isSubmitting}>Войти</button>
                    <FormError name='login' message={errors.login} />
                </form>
            </main>
        </div>
    );
};

export default Login;