/* eslint-disable no-undef */
/* eslint-disable testing-library/no-unnecessary-act */
/* eslint-disable testing-library/prefer-screen-queries */

import React from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { render, fireEvent, act } from '@testing-library/react'
import { MemoryRouter } from 'react-router-dom';

import Login from '../Login/Login';
import { login } from '../../redux/actions/index';

const renderComponent = () => render(<Login />, { wrapper: MemoryRouter });

jest.mock('react-redux');

describe('Login page', () => {
    it('Should be able to do login', async () => {
        useSelector.mockImplementation(cb =>
            cb({
                user: {
                    data: null,
                    loading: false,
                    error: null,
                }
            })
        )

        const { getByTestId } = renderComponent()
        const dispatch = jest.fn()

        useDispatch.mockReturnValue(dispatch)

        await act(async () => {
            fireEvent.change(getByTestId('login-email'), {
                target: { value: 'admin@email.com' }
            })
        });

        await act(async () => {
            fireEvent.change(getByTestId('login-password'), {
                target: { value: 'QWEqwe123' }
            })
        });

        await act(async () => {
            fireEvent.submit(getByTestId('login__form'))
        });

        expect(dispatch).toHaveBeenCalledWith(
            login({ email: 'admin@email.com', password: 'QWEqwe123' })
        )
    })
})

export { }