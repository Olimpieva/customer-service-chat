import React from "react";

import FormError from "../FormError/FormError";

import "./InputField.css";

function InputField(props) {
    const { type, formName, name, title, value, onChange, onFocus, error } = props;

    return (
        <fieldset className={`input-field ${formName}__input-field`}>
            <label className="input-field__caption" htmlFor={`${formName}-${name}`} >{title}</label>
            <input className="input-field__input" id={`${formName}-${name}`} data-testid={`${formName}-${name}`}
                type={type}
                name={name}
                value={value}
                onChange={onChange}
                onFocus={onFocus}
            />
            <FormError
                name={name}
                message={error}
            />
        </fieldset>
    )
}

export default InputField;