const firebaseConfig = {
    apiKey: "AIzaSyD2QAFHOwnAyTcwIO8XnXeZZIfiWXAd7A0",
    authDomain: "customer-service-chat.firebaseapp.com",
    databaseURL: "https://customer-service-chat-default-rtdb.europe-west1.firebasedatabase.app",
    projectId: "customer-service-chat",
    storageBucket: "customer-service-chat.appspot.com",
    messagingSenderId: "116084114208",
    appId: "1:116084114208:web:9ff5a558739a18e24a8d45",
    measurementId: "G-4XXFNG7C7J"
};

export default firebaseConfig;