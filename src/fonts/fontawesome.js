import { library } from '@fortawesome/fontawesome-svg-core';
import { faVk, faGoogle } from '@fortawesome/free-brands-svg-icons';
import { faCoffee } from '@fortawesome/free-solid-svg-icons'

library.add(
    faVk,
    faGoogle,
    faCoffee
);