import * as yup from "yup";

import { inputPatterns, validationErrorMessages } from "../utils/constants";

export const loginValidationSchema = yup.object().shape({
    email: yup
        .string()
        .required(validationErrorMessages.valueMissing('Email'))
        .matches(
            inputPatterns.email,
            validationErrorMessages.emailPatternMismatch()
        ),
    password: yup
        .string()
        .required(validationErrorMessages.valueMissing('Password'))
        .matches(
            inputPatterns.password,
            validationErrorMessages.passwordPatternMismatch()
        ),
});