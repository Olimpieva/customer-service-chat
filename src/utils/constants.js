export const inputPatterns = {
    email: /(?!(^[.-].*|[^@]*[.-]@|.*\.{2,}.*)|^.{254}.)([a-zA-Z0-9!#$%&'*+/=?^_`{|}~.-]+@)(?!-.*|.*-\.)([a-zA-Z0-9-]{1,63}\.)+[a-zA-Z]{2,15}/,
    password: /(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,30}/,
};

export const validationErrorMessages = {
    valueMissing: (name) => `${name} is required`,
    emailPatternMismatch: () => 'Invalid email',
    passwordPatternMismatch: () => 'Invalid password. A password must contain at least 8 characters, including at least 1 number and includes both lower and uppercase letters',
};

export const requestErrorMessages = {
    invalidUserData: {
        message: 'Вы ввели неправильный логин или пароль.'
    },
    userAlreadyExist: {
        firebaseError: 'auth/email-already-exists',
        message: 'Пользователь с таким email уже существует.'
    },
    serverError: {
        message: 'На сервере произошла ошибка. Подождите немного и попробуйте ещё раз.'
    },
    pageNotFound: {
        message: 'Страница не найдена.'
    },
};