import { requestErrorMessages } from "./constants";

const handleError = (errorData) => {

    let error = requestErrorMessages.serverError.message;

    if (errorData.code.includes('auth')) {

        switch (errorData.code) {
            case requestErrorMessages.userAlreadyExist.firebaseError:
                error = requestErrorMessages.userAlreadyExist.message;
                break;
            default: error = requestErrorMessages.invalidUserData.message;
        }
    }

    return error;
};

export default handleError; 