import React from 'react';
import { Provider } from 'react-redux';
import { createRoot } from "react-dom/client";
import { BrowserRouter } from 'react-router-dom';
import { initializeApp } from "firebase/app"

import store from './redux';
import App from './components/App/App';

import './index.css';
import firebaseConfig from './firebaseConfig';

export const firebaseApp = initializeApp(firebaseConfig);

const rootElement = document.getElementById("root") || document.createElement('div');
const root = createRoot(rootElement);

root.render(
  <Provider store={store}>
    <BrowserRouter>
      <App />
    </BrowserRouter>
  </Provider>
);

