import { FAILURE, LOGIN, REQUEST, SUCCESS } from "./actionTypes";

export const login = (payload) => ({ type: LOGIN + REQUEST, payload });
export const loginSuccess = (payload) => ({ type: LOGIN + SUCCESS, payload });
export const loginFailure = (payload) => ({ type: LOGIN + FAILURE, payload });