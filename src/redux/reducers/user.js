
import { LOGIN, FAILURE, REQUEST, SUCCESS, } from "../actions/actionTypes";

const initialState = {
    data: null,
    loading: false,
    error: null,
};

const user = (state = initialState, action) => {

    switch (action.type) {
        case LOGIN + REQUEST:
            return { ...state, loading: true, error: null };
        case LOGIN + SUCCESS:
            return { ...state, data: action.payload, loading: false, error: null };
        case LOGIN + FAILURE:
            return { ...state, data: null, loading: false, error: action.payload };
        default:
            return state;
    };
};

export default user;