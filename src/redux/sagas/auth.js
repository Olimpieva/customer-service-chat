import { getAuth, signInWithEmailAndPassword } from "firebase/auth";
import { put, call, takeLatest } from 'redux-saga/effects';

import { loginFailure, loginSuccess } from "../actions";
import { LOGIN, REQUEST } from "../actions/actionTypes";
import handleError from "../../utils/errorHandler";

export function* loginWorkerSaga({ type, payload: { email, password } }) {

    const auth = getAuth();

    try {
        const { user } = yield call(signInWithEmailAndPassword, auth, email, password);

        localStorage.setItem('jwt', user.accessToken);

        yield put(loginSuccess({ id: user.uid, email: user.email }));
    } catch (error) {
        yield put(loginFailure(handleError(error)));
    }
};

export function* loginWatcherSaga() {
    yield takeLatest(LOGIN + REQUEST, loginWorkerSaga);
};