import { all, fork } from 'redux-saga/effects';

import { loginWatcherSaga } from './auth';

export default function* rootSaga() {
    yield all([
        fork(loginWatcherSaga),
    ]);
};